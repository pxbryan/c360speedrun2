﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Multi Tour Menu")]
	public class MultiTourMenu : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Scenes")]
		[SerializeField]
		protected List<string> sceneNames;

		[Header("Branding")]
		[SerializeField]
		protected Color buttonFillColour = DigitalSalmon.Colours.BLUE;

		[SerializeField]
		protected TourLoadButton buttonTemplate;

		[Header("Assignment")]
		[SerializeField]
		protected Transform buttonsContainer;

		[SerializeField]
		protected FadePostProcess fade;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() {
			foreach (string sceneName in sceneNames) {
				CreateTourButton(sceneName);
			}
		}

		protected void Start() { fade.FadeUp(); }

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private void CreateTourButton(string scene) {
			if (!Application.CanStreamedLevelBeLoaded(scene)) {
				Debug.LogWarning($"Scene '{scene}' couldn't be found.");
			}

			TourLoadButton button = Instantiate(buttonTemplate, buttonsContainer);
			button.SetTargetScene(scene);
			button.SetText(scene);
		}
	}
}