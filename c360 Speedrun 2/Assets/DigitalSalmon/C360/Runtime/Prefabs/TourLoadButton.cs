﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Tour Load Button")]
	public class TourLoadButton : PrefabButton {
		public event EventHandler<TourLoadButton> Selected;

		[SerializeField]
		protected string targetScene;

		public string TargetScene => targetScene;

		protected override void Popup_Triggered(Hotspot value) {
			base.Popup_Triggered(value);
			Selected.InvokeSafe(this);
			FadePostProcess fadePostProcess = FindObjectOfType<FadePostProcess>();
			if (fadePostProcess != null) fadePostProcess.FadeDown(onComplete: () => SceneManager.LoadScene(TargetScene));
			else SceneManager.LoadScene(TargetScene);
		}

		public void SetTargetScene(string sceneName) { this.targetScene = sceneName; }
	}
}