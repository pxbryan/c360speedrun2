﻿using DigitalSalmon.Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Popup Video")]
	public class PopupVideo : Popup {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Video Settings")]
		[SerializeField]
		protected VideoClip videoClip;

		[SerializeField]
		protected string videoURL;

		[Header("Popup Settings")]
		[SerializeField]
		protected bool pauseOnUnHover;

		[SerializeField]
		protected bool restartOnHover;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private UnityVideoController videoController;
		private RawImage rawImage;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected override void Awake() {
			base.Awake();
			LocateComponents();
		}

		protected override void OnEnable() {
			base.OnEnable();
			videoController.TargetTextureChanged += VideoController_TargetTextureChanged;
		}

		protected override void OnDisable() {
			base.OnDisable();
			videoController.TargetTextureChanged -= VideoController_TargetTextureChanged;
		}

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		private void VideoController_TargetTextureChanged() { rawImage.texture = videoController.TargetTexture; }

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public void Play() {
			if (videoClip != null) videoController.PlayClip(videoClip);
			else videoController.PlayURL(videoURL);
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected override void HoveredChanged(bool hovered) {
			base.HoveredChanged(hovered);
			if (hovered) {
				if (!restartOnHover && videoController.IsPlaying) return;
				Play();
			}
			else if (pauseOnUnHover) {
				videoController.Pause();
			}
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private void LocateComponents() {
			rawImage = GetComponentInChildren<RawImage>();
			videoController = this.GetOrAddComponent<UnityVideoController>();
		}
	}
}