﻿using System.Collections;
using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Input/Gaze Input Raycaster")]
	public class GazeInputRaycaster : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Protected Fields:
		//-----------------------------------------------------------------------------------------

		protected LayerMask exclusionLayers = 0; // Layers to exclude from the raycast.
		protected float rayLength = 20f; // How far into the scene the ray is cast.

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public IInputBroadcaster CurrentInteractible { get; private set; }
		public IInputBroadcaster LastInteractible { get; private set; } //The last interactive item

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Start() { StartCoroutine(RaycastLoopCoroutine()); }

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private IEnumerator RaycastLoopCoroutine() {
			while (true) {
				yield return new WaitForEndOfFrame();
				PhysicsRaycast();
			}
		}

		private void PhysicsRaycast() {
			// Create a ray that points forwards from the camera.
			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit hit;

			// Do the raycast forweards to see if we hit an interactive item
			if (Physics.Raycast(ray, out hit, rayLength)) {
				IInputBroadcaster interactible = hit.collider.GetComponentInChildren<IInputBroadcaster>(); //attempt to get the VRInteractiveItem on the hit object
				CurrentInteractible = interactible;

				// If we hit an interactive item and it's not the same as the last interactive item, then call Over
				if (interactible != null && interactible != LastInteractible) interactible.BeginInput();

				// Deactive the last interactive item 
				if (interactible != LastInteractible) DeactiveLastInteractible();

				LastInteractible = interactible;
			}
			else {
				// Nothing was hit, deactive the last interactive item.
				DeactiveLastInteractible();
				CurrentInteractible = null;
			}
		}

		private void DeactiveLastInteractible() {
			if (LastInteractible == null) return;

			LastInteractible.EndInput();
			LastInteractible = null;
		}
	}
}