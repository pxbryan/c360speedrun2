﻿namespace DigitalSalmon.C360 {
	public class PrefabTimelineVisibilityBehaviour : TimelineVisibilityBehaviour, IMappedPrefab {
		//-----------------------------------------------------------------------------------------
		// Private Properties:
		//-----------------------------------------------------------------------------------------

		private PrefabElement Element { get; set; }

		//-----------------------------------------------------------------------------------------
		// Interface Methods:
		//-----------------------------------------------------------------------------------------

		void IMappedPrefab.UpdateState(MediaSwitchStates state) { }

		void IMappedPrefab.UpdateData(PrefabElement element, Node node) {
			Element = element;
			if (timelineVisibility != null) StopCoroutine(timelineVisibility);

			if (Element == null) return;
			timelineVisibility = TimelineVisibilityCoroutine(Element);
			StartCoroutine(timelineVisibility);
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected override void SetVisibility(bool visible) {
			base.SetVisibility(visible);
			transform.GetChild(0)?.gameObject?.SetActive(visible);
		}
	}
}