﻿using System.Collections;
using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Input/Mouse Input Raycaster")]
	public class MouseInputRaycaster : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Protected Fields:
		//-----------------------------------------------------------------------------------------

		protected LayerMask exclusionLayers = 0; // Layers to exclude from the raycast.
		protected float rayLength = 50f; // How far into the scene the ray is cast.

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private Camera rayCamera;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public IInputBroadcaster CurrentInteractable { get; private set; }
		public IInputBroadcaster LastInteractable { get; private set; }

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() { rayCamera = GetComponent<Camera>(); }

		protected void Start() { StartCoroutine(RaycastLoopCoroutine()); }

		protected void Update() {
			if (Input.GetMouseButtonDown(0)) {
				if (CurrentInteractable != null) {
					CurrentInteractable.SubmitInput();
				}
			}
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private IEnumerator RaycastLoopCoroutine() {
			while (true) {
				yield return new WaitForEndOfFrame();
				if (new Rect(0, 0, Screen.width, Screen.height).Contains(Input.mousePosition)) {
					Raycast();
				}
			}
		}

		private void Raycast() {
			Ray ray = rayCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			// Do the raycast forweards to see if we hit an interactive item
			if (Physics.Raycast(ray, out hit, rayLength)) {
				IInputBroadcaster interactible = hit.collider.GetComponentInChildren<IInputBroadcaster>(); //attempt to get the VRInteractiveItem on the hit object
				CurrentInteractable = interactible;

				// If we hit an interactive item and it's not the same as the last interactive item, then call Over
				if (interactible != null && interactible != LastInteractable) interactible.BeginInput();

				// Deactive the last interactive item 
				if (interactible != LastInteractable) DeactiveLastInteractible();

				LastInteractable = interactible;
			}
			else {
				// Nothing was hit, deactive the last interactive item.
				DeactiveLastInteractible();
				CurrentInteractable = null;
			}
		}

		private void DeactiveLastInteractible() {
			if (LastInteractable == null) return;

			LastInteractable.EndInput();
			LastInteractable = null;
		}
	}
}