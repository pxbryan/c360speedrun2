﻿using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Hotspot Prefab Button")]
	public class HotspotPrefabButton : PrefabButton {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[SerializeField]
		protected string targetMedia;

		protected override void Popup_Triggered(Hotspot value) {
			base.Popup_Triggered(value);
			Complete360Tour.GoToMedia(targetMedia);
		}
	}
}