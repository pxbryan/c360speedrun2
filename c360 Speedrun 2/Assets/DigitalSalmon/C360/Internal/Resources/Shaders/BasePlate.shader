﻿Shader "Complete360Tour/BasePlate"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Radius ("Radius", Range(0,1)) = 0.75
		_Softness ("Softness", Range(0.25,5)) = 1
	}
	SubShader
	{
		 Tags 
         { 
                 "Queue"="Geometry" 
                 "RenderType"="Opaque"
                 "ForceNoShadowCasting" = "True"
                 "IgnoreProjector"="True"
         }                
         
         Pass
         {
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Back
    
            ZWrite Off
            ZTest Off
             
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Radius;
			float _Softness;
			
			
            static const float DEG2RAD = 0.01745329252;
            
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			// 2D Circle
            float sdSphere(float2 domain, float radius) {
                return length(domain) - radius;
            }
            
            // Custom smoothing sampling distributed amongst the sign threshold.
            float sampleSmoothSigned(float field, float smoothing) {
                smoothing /= 100;
                return smoothstep(smoothing/2, -smoothing/2, field);
            }
            
            // Remap 0->1 uv space to -0.5 -> 0.5 uv space.
            float2 uvrQuad(float2 uv) {
                return uv - float2(0.5, 0.5);
            }
            
            // 2D Box - X|Y
            float sdBox(float2 domain, float2 size) {
                float2 d = abs(domain) - size;
                return min(max(d.x, d.y), 0.0) + length(max(d, 0.0));
            }

            
            // rotate 'domain' by 'degrees'.
            float2 opRotate(float2 domain, float degrees) {
                float s = sin(degrees * DEG2RAD);
                float c = cos(degrees * DEG2RAD);
            
                float tx = domain.x;
                float ty = domain.y;
                domain.x = (c * tx) - (s * ty);
                domain.y = (s * tx) + (c * ty);
                return domain;
            }
            

			fixed4 frag (v2f i) : SV_Target
			{
			    fixed4 col = fixed4(0,0,0,1);
				
				//Make radius slider sensible
				_Radius = (_Radius * 0.5) - 0.05;
				
				float2 radialUV = uvrQuad(i.uv);
				
				float mask = sdSphere(radialUV, _Radius);
				mask = sampleSmoothSigned(mask, _Softness);
				col.a = mask;
				return col;
			}

			ENDCG
		}
	}
}
