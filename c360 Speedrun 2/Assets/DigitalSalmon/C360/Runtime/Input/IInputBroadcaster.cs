﻿namespace DigitalSalmon.C360 {
	public interface IInputBroadcaster {
		event EventHandler OnInputBegin;
		event EventHandler OnInputEnd;
		event EventHandler OnInputSubmit;

		void BeginInput();
		void EndInput();
		void SubmitInput();
	}
}