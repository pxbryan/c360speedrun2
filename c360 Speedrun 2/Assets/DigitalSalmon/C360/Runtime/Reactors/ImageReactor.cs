﻿using System.Collections;
using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Core/Image Reactor")]
	public class ImageReactor : MediaReactor {
		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		protected override void C360_MediaSwitch(MediaSwitchStates state, Node node) {
			if (state == MediaSwitchStates.Switch) SwitchMedia(node);
		}

		//-----------------------------------------------------------------------------------------
		// Protected Methods:
		//-----------------------------------------------------------------------------------------

		protected void SwitchMedia(Node node) {
			if (!(node is ImageNode imageNode)) return;

			sequence.Cancel();

			
			Surface.SetTexture(imageNode.Media.Image);
			Surface.SetStereoscopic(imageNode.Media.IsStereoscopic);
			Surface.SetYFlip(false);
		
		}
	}
}