﻿using System.Collections;
using DigitalSalmon.UI;
using UnityEngine;

namespace DigitalSalmon.C360 {
	public class Time : Singleton<Time> {
		//-----------------------------------------------------------------------------------------
		// Public Events:
		//-----------------------------------------------------------------------------------------

		public static event EventHandler TimerReset;

		//-----------------------------------------------------------------------------------------
		// Serialized Fields:
		//-----------------------------------------------------------------------------------------

		[SerializeField]
		protected bool showTime;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public static float Seconds { get; private set; }

		private static Sequence Sequence { get; set; }

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() { Sequence = new Sequence(this); }

		protected void OnEnable() { Complete360Tour.MediaSwitch += Complete360Tour_MediaSwitch; }

		protected void OnGUI() {
			if (!showTime) return;
			Style.GUI.Box(new Rect(0, 0, 200, 40), DigitalSalmon.Colours.BLACK_60);
			string time = (Seconds*1000).ToString("0000");
			if (Complete360Tour.ActiveNode is ITimelineNode timelineNode) { time += $" / {timelineNode.DurationMs:0000}"; }
			GUILayout.Label(time);
		}

		protected override void OnDisable() {
			base.OnDisable();
			Complete360Tour.MediaSwitch -= Complete360Tour_MediaSwitch;
		}

		//-----------------------------------------------------------------------------------------
		// Event Handlers:
		//-----------------------------------------------------------------------------------------

		private void Complete360Tour_MediaSwitch(MediaSwitchStates state, Node node) {
			if (state == MediaSwitchStates.Switch) {
				ResetTimer();
				StartTimer();
			}
		}
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public static void ResetTimer() {
			Seconds = 0;
			StopTimer();
			TimerReset.InvokeSafe();
		}

		public static void StopTimer() { Sequence.Cancel(); }

		public static void StartTimer() {
			Sequence.Cancel();
			Sequence.Coroutine(TimerCoroutine());
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private static IEnumerator TimerCoroutine() {
			float startTime = UnityEngine.Time.timeSinceLevelLoad;
			while (true) {
				Seconds = UnityEngine.Time.timeSinceLevelLoad - startTime;
				CheckLoop();
				yield return null;
			}
		}

		private static void CheckLoop() {
			if (Complete360Tour.ActiveNode is ITimelineNode timelineNode) {
				if (timelineNode.Loop) {
					if (Seconds > timelineNode.DurationMs / 1000) {
						ResetTimer();
						StartTimer();
					}
				}
				else {
					float durationSeconds = (float) (timelineNode.DurationMs / 1000);
					if (Seconds > durationSeconds) Seconds = durationSeconds;
				}
			}
		}
	}
}