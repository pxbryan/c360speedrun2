﻿using UnityEngine;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Input/Input Broadcaster Behaviour")]
	public class InputBroadcasterBehaviour : BaseBehaviour, IInputBroadcaster {
		//-----------------------------------------------------------------------------------------
		// Public Events:
		//-----------------------------------------------------------------------------------------

		public event EventHandler OnInputBegin;
		public event EventHandler OnInputEnd;
		public event EventHandler OnInputSubmit;

		//-----------------------------------------------------------------------------------------
		// Interface Methods:
		//-----------------------------------------------------------------------------------------

		void IInputBroadcaster.BeginInput() { OnInputBegin.InvokeSafe(); }
		void IInputBroadcaster.EndInput() { OnInputEnd.InvokeSafe(); }
		void IInputBroadcaster.SubmitInput() { OnInputSubmit.InvokeSafe(); }
	}
}