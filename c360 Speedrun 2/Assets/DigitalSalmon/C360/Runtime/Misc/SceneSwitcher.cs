﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Scene Switcher")]
	public class SceneSwitcher : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Required")]
		[SerializeField]
		protected string sceneName = "ExampleTour"; // Scene must exist in Build Settings.

		[Header("Optional")]
		[SerializeField]
		protected FadePostProcess fade;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Update() {
			if (Input.GetKeyDown(KeyCode.Space)) {
				LoadScene();
			}
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private void LoadScene() {
			if (fade == null) {
				SceneManager.LoadScene(sceneName);
				return;
			}

			fade.FadedDown += () => SceneManager.LoadScene(sceneName);
			fade.FadeDown();
		}
	}
}