﻿using UnityEngine;
using UnityEngine.UI;

namespace DigitalSalmon.C360 {
	[AddComponentMenu("Complete 360 Tour/Examples/Base Plate")]
	public class BasePlate : BaseBehaviour {
		//-----------------------------------------------------------------------------------------
		// Inspector Variables:
		//-----------------------------------------------------------------------------------------

		[Header("Base Plate")]
		[Range(0.45f, 1)]
		[SerializeField]
		protected float size = 1f;

		[SerializeField]
		protected bool isBottom = true;

		[SerializeField]
		protected Material radialMask;

		[Header("Logos")]
		[Range(0.5f, 2)]
		[SerializeField]
		protected float logoSize = 1f;

		[SerializeField]
		protected Texture logo;

		[SerializeField]
		protected Transform logoContainer;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private RawImage[] logos;

		//-----------------------------------------------------------------------------------------
		// Unity Lifecycle:
		//-----------------------------------------------------------------------------------------

		protected void Awake() {
			SetTransform();
			SetRadialSize();
			GetLogoObjects();
			SetLogoSizes();
			SetLogos();
		}

		protected void OnValidate() {
			SetRadialSize();
			GetLogoObjects();
			SetLogoSizes();
			SetLogos();
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private void SetTransform() {
			transform.position = new Vector3(0, isBottom ? -5 : 5, 0);
			transform.rotation = Quaternion.Euler(isBottom ? 90 : -90, 0, 0);
		}

		private void SetRadialSize() {
			if (radialMask != null) radialMask.SetFloat("_Radius", size);

			if (logoContainer != null) logoContainer.localScale = Vector3.one * size;
		}

		private void GetLogoObjects() {
			if (logoContainer == null) return;

			logos = logoContainer.GetComponentsInChildren<RawImage>();
		}

		private void SetLogoSizes() {
			if (logos == null || logoContainer == null) return;

			Vector3 scale = Vector3.one * logoSize;
			foreach (RawImage logoObject in logos) {
				logoObject.transform.localScale = scale;
			}

			logoContainer.localScale = Vector3.one * size;
		}

		private void SetLogos() {
			if (logos == null || logo == null) return;

			foreach (RawImage logoImage in logos) {
				logoImage.texture = logo;
			}
		}
	}
}